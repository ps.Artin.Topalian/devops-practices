# Use a base image with Java and Maven
FROM adoptopenjdk:11-jdk-hotspot

# Install SDKMAN! and Maven
RUN apt-get update && apt-get install -y curl zip unzip
RUN curl -s "https://get.sdkman.io" | bash
RUN /bin/bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && sdk install maven"
